﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PrimerParcial
{
    class Program
    {
        static void Main(string[] args)
        {
            int saldo = 0;
            int billete1000 = 18;
            int billete500 = 19;
            int billete200 = 23;
            int billete50 = 100;
            int montoretirar = 0;

            // monto total del cajero
            saldo = (billete1000 * 1000) + (billete500 * 500) + (billete200 * 200) + (billete50 * 50);
            int opc;
            do
            {

                Console.WriteLine("\t************************");
                Console.WriteLine("\t*  1- Consultar balance  *");
                Console.WriteLine("\t*  2- Retirar efectivo.         *");
                Console.WriteLine("\t*  [0]- Salir                            *");
                Console.WriteLine("\t************************");
                Console.Write("Opcion: ");

                opc = Convert.ToInt32(Console.ReadLine());

                switch (opc)
                {
                    case 1:
                        saldo = (billete1000 * 1000) + (billete500 * 500) + (billete200 * 200) + (billete50 * 50);
                        Console.WriteLine("El saldo es: " + saldo);
                        break;
                    case 2:
                        if (saldo > 0)
                        {
                            Console.WriteLine("Digite el monto a retirar: ");
                            montoretirar = int.Parse(Console.ReadLine());
                            var lista = new List<string>();

                            if (montoretirar > 20000)
                            {
                                Console.WriteLine("El limite para retirar es 20,000 ");
                                continue;
                            }

                            if (montoretirar > saldo)
                            {
                                Console.WriteLine("Fondos insuficientes");
                                continue;
                            }

                            int billeteretirados = 0;
                            if (montoretirar >= 1000 && billete1000 > 0)
                            {
                                billeteretirados = (int)montoretirar / 1000;

                                if (billeteretirados > billete1000)
                                {
                                    billeteretirados = billete1000;
                                }
                                montoretirar = montoretirar - billeteretirados * 1000;
                                billete1000 = billete1000 - billeteretirados;
                                lista.Add("Cantidad de billetes de 1000 " + billeteretirados);
                            }

                            if (montoretirar >= 500 && billete500 > 0)
                            {
                                billeteretirados = (int)montoretirar / 500;

                                if (billeteretirados > billete500)
                                {
                                    billeteretirados = billete500;
                                }
                                montoretirar = montoretirar - billeteretirados * 500;
                                billete500 = billete500 - billeteretirados;
                                lista.Add("Cantidad de billetes de 500 " + billeteretirados);

                            }

                            if (montoretirar >= 200 && billete200 > 0)
                            {
                                billeteretirados = (int)montoretirar / 200;

                                if (billeteretirados > billete200)
                                {
                                    billeteretirados = billete200;
                                }
                                montoretirar = montoretirar - billeteretirados * 200;
                                billete200 = billete200 - billeteretirados;
                                lista.Add("Cantidad de billetes de 200 " + billeteretirados);
                            }

                            if (montoretirar >= 50 && billete50 > 0)
                            {
                                billeteretirados = (int)montoretirar / 50;

                                if (billeteretirados > billete50)
                                {
                                    billeteretirados = billete50;
                                }
                                montoretirar = montoretirar - billeteretirados * 50;
                                billete50 = billete50 - billeteretirados;
                                lista.Add("Cantidad de billetes de 50 " + billeteretirados);
                            }

                            foreach (var item in lista)
                            {
                                Console.WriteLine(item);
                            }

                            saldo = saldo - montoretirar;
                        }
                        else
                        {
                            Console.WriteLine("Saldo insuficientes");
                        }
                        break;

                }
            }
            while (opc != 0);







        }
    }
}